import React, { PropTypes } from 'react';

const style = `
  .root {
    padding-left: 20px;
    padding-right: 20px;
  }
`;

export default class extends React.Component {
  render() {
    return (
      <div className={style.root}>
        <div className={style.container}>
          <h1>Hi</h1>
        </div>
      </div>
    );
  }
}

import React from 'react';
import Home from '../components/home';

export default {
  path: '/',

  async action() {
    return {
      title: 'Home',
      description: 'ololo',
      component: <Home />,
    };
  }
};

import express from 'express';
import path from 'path';
import UniversalRouter from 'universal-router';
import React from 'react';
import ReactDOM from 'react-dom/server';

import App from './components/app';
import Html from './components/html';
import homeRoute from './routes/home';

const app = express();
app.use(express.static(path.join(__dirname, '..','static')));

const routes = {
  path: '/',
  children: [ homeRoute ],

  async action({ next }) {
    const route = await next();
    try {
      route.title = route.title || 'Untitled Page';
      route.description = route.description || '';
    } catch (err) { }
    return route;
  }
}

app.get('/', async (req, res, next) => {
    const css = new Set();

    const context = {
      insertCss: (...styles) => {
        styles.forEach(style => css.add(style._getCss()));
      }
    };

    const route = await UniversalRouter.resolve(routes, {
      path: req.path,
      query: req.query,
    });

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = { ...route };
    data.children = ReactDOM.renderToString(<App context={context}>{route.component}</App>);
    data.styles = [{ id: 'css', cssText: [...css].join('') }];
    data.scripts = [ 'client.js' ];

    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);
    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
});

app.use((err, req, res, next) => {
  console.error(err);

  let json = JSON.stringify({
    status: err.status || 404,
    message: 'Internal Server Error'
  }, null, 2);

  res.status(json.status).send(json);
});

let server = app.listen(80, () => {
  console.log(`The server is running at http://localhost:${server.address().port}/`);
});
